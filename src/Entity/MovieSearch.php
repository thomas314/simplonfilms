<?php

namespace App\Entity;

class MovieSearch {

    private $Moviesearch;
    
    
    public function getMoviesearch(): ?string
    {
        return $this->Moviesearch;
    }
    
    public function setMoviesearch(?string $Moviesearch): self
    {
        $this->Moviesearch = $Moviesearch;
    
        return $this;
    }
}
