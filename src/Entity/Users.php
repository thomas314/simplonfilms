<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsersRepository")
 * @UniqueEntity(
 * fields= {"email"},
 * message= "Cet email est déjà utilisé !"
 * )
 */
class Users implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * 
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="8", minMessage="Votre mot de passe doit contenir au moins 8 caractères")
     *      
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Vous n'avez pas tapé le même mot de passe")
     */

    public $confirm_password;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Orders", mappedBy="user_id", orphanRemoval=true)
     */
    private $orders;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RolesUsers", mappedBy="user")
     */
    private $rolesUsers;


    private $roles = [];


    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->rolesUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getUsername(): ?string {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials() {}

    public function getSalt() {}

    public function getRoles(): array {
            //pour attribuer un rôle USER par défaut
            $roles[] = 'ROLE_USER';
            $this->roles = $roles;

            //pour déterminer le rôle de la personne qui se connecte
            $rolesX = $this->roles;
            foreach( $this->getRolesUsers() as $roleUser )
            {
                $rolesX[] = $roleUser->getRole()->getSlug();
            } 
            return array_unique($rolesX);
        }

    /**
     * @return Collection|Orders[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Orders $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUserId($this);
        }
        return $this;
    }

    public function removeOrder(Orders $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getUserId() === $this) {
                $order->setUserId(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|RolesUsers[]
     */
    public function getRolesUsers(): Collection
    {
        return $this->rolesUsers;
    }

    public function addRolesUser(RolesUsers $rolesUser): self
    {
        if (!$this->rolesUsers->contains($rolesUser)) {
            $this->rolesUsers[] = $rolesUser;
            $rolesUser->setUser($this);
        }

        return $this;
    }

    public function removeRolesUser(RolesUsers $rolesUser): self
    {
        if ($this->rolesUsers->contains($rolesUser)) {
            $this->rolesUsers->removeElement($rolesUser);
            // set the owning side to null (unless already changed)
            if ($rolesUser->getUser() === $this) {
                $rolesUser->setUser(null);
            }
        }

        return $this;
    }
    
    public function __toString(){
        return $this->name;
    }
}
