<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\MoviesOrders;
use App\Entity\Movies;
use App\Entity\Users;
use App\Entity\Orders;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class OrdersController extends AbstractController
{
    /**
     * @Route("/showOrder", name="showOrder")
     */
    // affichage de la liste des films empruntés 
    public function index()
    {
        return $this->render('orders/index.html.twig', [
            'controller_name' => 'OrdersController',
        ]);
    }

      /**
     * @Route("/order", name="order")
     */
    public function validateOrder( Request $request, SessionInterface $session, ObjectManager $manager  )
    {        
        
        $sessionUser = $session->get('sessionUser', []);
        $sessionUser['sessionUser'] = $this->getUser()->getId();
        $session->set('sessionUser', $sessionUser);
        //dd($session->get('sessionUser'));


       // $session['user']= 1;
        $repo_users = $this->getDoctrine()->getRepository(Users::class);
        $userslogged = $repo_users->find($session->get('sessionUser')['sessionUser']);

        $userslogged_id = $userslogged->getId();

        //-- Create order

        $order = new Orders;
        $order->setReference(rand(1000000, 9999999));
        $order->setUserId($userslogged);      // app/user         $session['user']
        $manager->persist($order);
        $manager->flush();


        $panier = $session->get('panier');

        foreach( $panier as $movie_id => $movie_qty){


            $movieOrder = new MoviesOrders;
            $movieOrder->setOrders($order);
            $moviesrepo = $this->getDoctrine()->getRepository(Movies::class);
            $movieOrder->setMovie( $moviesrepo->find($movie_id) );
            //$movieOrder->setQuantity($movie_qty);
            $manager->persist($movieOrder);
        }
        $manager->flush();

        foreach ($panier as $movie_id => $movie_qty){
            $moviesrepo = $this->getDoctrine()->getRepository(Movies::class);
            $movieToUpdate = $moviesrepo->find($movie_id);
            $movieToUpdate->setavailable(0);

            $manager->persist($movieToUpdate);
        }
        $manager->flush();

        $session->clear();
        // die;


        return $this->redirectToRoute('home');
    }

     /**
     * @Route("/returnMovie/{id}", name="returnMovie")
     */
    //A COMPLETER 
    public function returnMovie()
    {
        return $this->render('orders/index.html.twig', [
            'controller_name' => 'OrdersController',
        ]);
    }
}
