<?php

namespace App\Controller;

use App\Repository\MoviesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index(SessionInterface $session, MoviesRepository $moviesRepository)
    {
        $panier = $session->get('panier', []);
        $panierData = [];

        foreach ($panier as $id => $quantity) {
            $panierData[] = [
                'film' => $moviesRepository->find($id) ,
                'quantity' => $quantity
            ];
        };

        $total = 0;

        // foreach ($panierData as $film) {
            // $totalfilm = $film['film']->getPrice() * $film['quantity']; 
            // $total += $totalfilm;
        // }
        return $this->render('cart/index.html.twig', [
            // 'controller_name' => 'CartController',
            'films' => $panierData
            // 'total' => $total
        ]);
    }
    /**
     * @Route("/cart/add/{id}", name="add_film")
     */
    public function add_film($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);
        if (!empty($panier[$id])) {
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }
        $session->set('panier', $panier);
        // dd($session->get('panier'));
        {
            return $this->redirectToRoute('home');
        }
    }
// Thomas
    /**
     * @Route("/cart/remove", name="remove_all")
     */
    public function remove_all(SessionInterface $session)
    {
        $session->clear();
        return $this->redirectToRoute('cart');
    } 
}
